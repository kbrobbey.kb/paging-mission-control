from datetime import datetime, timedelta
import json


class TelemetryPagingControl:
    def __init__(self):
        self.tupleMap = {}

    def ingestFile(self, textfile):
        keys = ["timestamp", "satelliteId", "redHighLimit", "yellowHighLimit",
                "yellowLowLimit", "redLowLimit", "rawValue", "component"
                ]

        for data in open(textfile, 'r'):
            values = data[:-1].split("|")
            values[0] = self.convertStringToDateTime(values[0])
            tempDictionary = dict(zip(keys, values))

            satelliteId = values[1]
            component = values[7]
            satelliteComponent = (satelliteId, component,)

            if satelliteComponent in self.tupleMap:
                self.tupleMap[satelliteComponent].append(tempDictionary)
            else:
                self.tupleMap[satelliteComponent] = [tempDictionary]

    def getReadings(self):
        for k in self.tupleMap:
            satelliteReadings = self.tupleMap[k]
            satelliteReadings = self.sortList(satelliteReadings, 'timestamp')
            if k[1] == 'TSTAT':
                result = self.checkThermostatReading(satelliteReadings)
                severityType = "RED HIGH"
            else:
                result = self.checkBatteryReading(satelliteReadings)
                severityType = "RED LOW"

            if result:
                datetimeString = self.getDatetimeReadable(satelliteReadings[0]['timestamp'])

                finalResut = {
                    "satelliteId": satelliteReadings[0]['satelliteId'],
                    "severity": severityType,
                    "component": satelliteReadings[0]['component'],
                    "timestamp": datetimeString
                }
                finalResultJSONObj = json.dumps(finalResut, indent=4)
                print(finalResultJSONObj)


    def sortList(self, inputList, field):
        return sorted(inputList, key=lambda x: x.get(f'{field}'))


    def getDatetimeReadable(self, dateTimeInput):
        return dateTimeInput.isoformat(timespec='milliseconds').replace('+00:00', 'Z')

    def convertStringToDateTime(self, timestamp):
        timestamp_date = timestamp[0:timestamp.index(' ')]
        timestamp_time = timestamp[timestamp.index(' ') + 1:].replace('.', ':').split(":")

        timestamp_date = {'year': timestamp_date[0:4], 'month': timestamp_date[4:6], 'day': timestamp_date[6:8]}
        timestamp_time = {"hour": timestamp_time[0], "minute": timestamp_time[1], "seconds": timestamp_time[2],
                          "milliseconds": timestamp_time[3]}

        datetimestring = f"{timestamp_date.get('year')}-{timestamp_date.get('month')}-{timestamp_date.get('day')}T" \
                         f"{timestamp_time.get('hour')}:{timestamp_time.get('minute')}:" \
                         f"{timestamp_time.get('seconds')}.{timestamp_time.get('milliseconds')}Z"

        formatedDatetime = datetime.strptime(datetimestring, '%Y-%m-%dT%H:%M:%S.%f%z')
        return formatedDatetime

    def checkBatteryReading(self, satelliteReadings):
        if len(satelliteReadings) <= 1:
            return False
        fiveMinutesFromNow = satelliteReadings[0]['timestamp'] + timedelta(minutes=5)
        redLowLimit = float(satelliteReadings[0]['redLowLimit'])
        redLowLimitCounter = 0
        for reading in satelliteReadings:
            reading_timestamp = reading['timestamp']
            reading_rawValue = float(reading['rawValue'])
            if reading_timestamp > fiveMinutesFromNow:
                break
            if redLowLimitCounter > 2:
                break
            if reading_rawValue < redLowLimit:
                redLowLimitCounter += 1
        return redLowLimitCounter > 2

    def checkThermostatReading(self, satelliteReadings):
        if len(satelliteReadings) <= 1:
            return False
        fiveMinutesFromNow = satelliteReadings[0]['timestamp'] + timedelta(minutes=5)
        redHighLimit = float(satelliteReadings[0]['redHighLimit'])
        redHighLimitCounter = 0
        for reading in satelliteReadings:
            reading_timestamp = reading['timestamp']
            reading_rawValue = float(reading['rawValue'])
            if reading_timestamp > fiveMinutesFromNow:
                break
            if redHighLimitCounter > 2:
                break
            if reading_rawValue > redHighLimit:
                redHighLimitCounter += 1
        return redHighLimitCounter > 2


thisinput = TelemetryPagingControl()
thisinput.ingestFile('input.txt')
thisinput.getReadings()